## Import ##

import time, sys, os, logging, datetime, select
import threading, multiprocessing, argparse 
import subprocess, dronekit, pymavlink, random
from logger import swarm_logger

## Swarm Manager object definition ##

class swarmManager( multiprocessing.Process ):
	def __init__(self, log_l, uav_n, ns, d_l, d_e_l, d1, d2, group = None, target = None, name = "swarmManager", args = (), kwargs = {}):
		self.log_l = log_l
		self.uav_n = uav_n
		self.ns = ns
		self.dl = d_l
		self.de_l = d_e_l
		self.d1 = d1
		self.d2 = d2
		self.name = name
		swarm_logger.info('[%s] initialized', self.name)
		super(swarmManager, self).__init__(group, target, name, args, kwargs)

	def run(self):
		swarm_logger.info('[%s] starting', self.name)
		# wait until main loop initialization completes
		while not self.ns.running:
			time.sleep(0.25)
			continue
		# running control loop
		swarm_logger.info('[%s] running', self.name)
		while self.ns.running: 
			#print "[DEBUG] %s working" % (self.name)
			## use the following to send messages to drones'ip directly from the manager
			#for d in self.dl:
			#	print "[INFO] Sending msg to %s" % (base_ip+str(d))
			## use the following to send messages to drone processes
			#for ev in self.de_l:
			#	if ev is not 0:
			#		self.d1 = random.randint(1, 10)
			for i in range(self.uav_n):
				if self.de_l[i]:
					swarm_logger.debug('[%s] drone:%s d1:%s d2:%s', self.name, i, self.d1[i], self.d2[i] )
					if self.log_l == logging.DEBUG: 
						print "[DEBUG][%s] drone:%s d1:%s d2:%s" % (self.name, i, self.d1[i],self.d2[i])
						print "[DEBUG][%s] drone:%s d2.gas0:%s" % (self.name, i, self.d2[i]['gas0'])
					self.de_l[i] = 0
			time.sleep(0.001)
		swarm_logger.info('[%s] closing', self.name)
		if self.log_l >= logging.DEBUG: print "[INFO][%s] closing" % (self.name)
		return

## ------ Class End ------ ##

