## Imports ##

import logging

## Constants ##

uav_n   = 2							# number of drones in the swarm
ctrl_l  = 0							# swarm control law identifier
fname   = "droneSwarmMng"			# name of the log file
log_l   = logging.WARNING			# log level
ver     = 'v0.1'					# program version
base_ip = "192.168.0."				# base ip address for ad-hoc network
init_ip = 101                       # last octet for ip used by drones, to obtain an ipv4 address base_ip.init_ip
base_ctrl_port  = 14550				# base ip:port form mavlink
base_sens_port  = 5760				# base ip:port for sensing task