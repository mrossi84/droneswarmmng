## Import ##

import time, sys, os, logging, datetime, select
import threading, multiprocessing, argparse 
import subprocess, dronekit, pymavlink, random
import aDroneIF, swarmManager
from logger import swarm_logger
from swarmGlobals import uav_n, ctrl_l, fname, log_l, ver, base_ip, init_ip, base_ctrl_port, base_sens_port

## Function that parses input command ##

def runCommand(command):
	res = 0
	# First check
	if command == '' or command == '\n' or command == '\r':
		pass
	# Close the system correctly
	elif command == 'exit':
		res = 1
	# print the help
	else:
		print '-----------------\n\r',\
			'available command\n\r',\
			'exit - closes sockets and exit\n\r',\
			'-----------------\n\r'
		pass
	return res

## ------ Section End ------ ##

## Entry Point ##

if __name__ == "__main__":

	mgr   = multiprocessing.Manager()
	drone_id_list = mgr.list()			# shared list of drone ids
	drone_evnt_list = mgr.list()        # shared list of drone events
	drone_d1_list = mgr.list()			# shared list for data exchange
	drone_d2_list = mgr.list()          # shared list for data exchange
	d_l = []	 						# list of drone objects managed by main process
	g_var = mgr.Namespace()
	g_var.running = False				# keyboard

	parser = argparse.ArgumentParser()
	parser.add_argument('-n', '--number_d', type=int, action='store', dest='number_d', 
                         help='Set the number of drones in the swarm', default=3)
	parser.add_argument('-c', '--ctrl_law', type=int, action='store', dest='ctrl_law',
					 help='Select the swarm control law, 0:mapping (default), 1:fleet', default=0)
	parser.add_argument('-l', '--log_level', type=int, action='store', dest= 'loglevel', 
                         help='Define log level: 2-debug, 1-info, default warning', default=logging.WARNING)
	parser.add_argument('--version', action='version', version='%(prog)s '+ver)
	param_list = parser.parse_args()

	if param_list.loglevel == 2:
		log_l = logging.DEBUG
	elif param_list.loglevel == 1:
		log_l = logging.INFO
	else:
		log_l = logging.WARNING
	logging.info('Set log level to: %s', log_l)

	uav_n  = param_list.number_d
	swarm_logger.info('Set size of swarm to: %s', uav_n)
	ctrl_l = param_list.ctrl_law
	swarm_logger.info('Set control law to: %s', ctrl_l)

	print '[INFO] Starting %s %s with params %d %d %d' % (fname,ver,log_l,uav_n,ctrl_l)

	try:
		# ping each drone to see if they've joined the network
		with open("null.txt","w") as null_f:
			for ping in range(uav_n):
				#address = base_ip + str(ping+init_ip)
				#res = subprocess.call(['ping', '-n', '1', address], stdout=null_f, stderr = null_f )
				#if res == 0:
				#	print "[INFO] ping to", address, "OK"
				#	swarm_logger.info("ping to %s : OK", address)
				#	d = aDrone(ping,args=(1,2,3))
				#	drone_id_list.append(d)
				#	d.start()
				#elif res == 2:
				#	print "[INFO] no response from", address
				#	swarm_logger.info("ping to %s : NO-RESP", address)
				#else:
				#	print "[INFO] ping to", address, "failed!"
				#	swarm_logger.info("ping to %s : FAIL", address)
				drone_evnt_list.append(0)
				drone_d1_list.append(0)
				drone_d2_list.append(0)
				d = aDroneIF.aDroneIF(log_l, base_ip, init_ip, base_ctrl_port, base_sens_port, ping, g_var, drone_evnt_list, drone_d1_list, drone_d2_list)
				d_l.append(d)
				drone_id_list.append(ping)
				d.start()

		sm_p = swarmManager.swarmManager(log_l, uav_n, g_var, drone_id_list, drone_evnt_list, drone_d1_list, drone_d2_list)
		sm_p.start()

		g_var.running = True

		while g_var.running:
			in_c = sys.stdin.readline().strip()
			if not (in_c == '' or in_c == ""):
				swarm_logger.info("input: %s", in_c)
				if runCommand(in_c) > 0:
					g_var.running = 0
			time.sleep(0.1)
	except KeyboardInterrupt:
		g_var.running = 0
		swarm_logger.warning('Keyboard interrupt! Safely Closing')
		print("Keyboard Interrupt! Safely Closing")
	except Exception, e:
		print 'Exception', e

	for i in d_l:
		i.join()

	sm_p.join()

	swarm_logger.info("Done")
	print 'End'